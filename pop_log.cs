using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pop_log : MonoBehaviour {
    public GameObject Prefab;
    bool canSpawnLog = true;
    bool isTreeDown = false;
    void OnCollisionEnter(Collision col)
    {
        if (canSpawnLog){
            StartCoroutine(mainFunction(col));
        }
    }
    
    public IEnumerator mainFunction(Collision col)
    {
        if (col.gameObject.name == "axe" && col.relativeVelocity.magnitude > 6)
        {

            canSpawnLog = false;

            //Create log
            GameObject PrefabRef = Instantiate(Prefab, transform.position, transform.rotation);
            //changes log layer to log(Doesn't collide with tree)
            PrefabRef.layer = LayerMask.NameToLayer("Logs");
           
            
            

            //Places log above tree
            PrefabRef.transform.position += 3 * Vector3.up;


            MeshCollider gameObjectsMeshCollider = PrefabRef.GetComponent<MeshCollider>();
            if (gameObjectsMeshCollider == null){
                gameObjectsMeshCollider = PrefabRef.AddComponent<MeshCollider>();
                gameObjectsMeshCollider.convex = true;
                gameObjectsMeshCollider.enabled = true;
            }
           
            //Adds physics to log + creates mass
            Rigidbody gameObjectsRigidBody = PrefabRef.GetComponent<Rigidbody>();
            if (gameObjectsRigidBody == null){
                gameObjectsRigidBody = PrefabRef.AddComponent<Rigidbody>();
                gameObjectsRigidBody.mass = 2;

                //Moves log after spawn
                gameObjectsRigidBody.AddForce(transform.up * 200);
            }
            

            
            //Makes object grabbable
            OVRGrabbable gameObjectsGrabbable = PrefabRef.GetComponent<OVRGrabbable>();
            if (gameObjectsGrabbable == null){
                gameObjectsGrabbable = PrefabRef.AddComponent<OVRGrabbable>();

                Collider[] newGrabPoints = new Collider[1];
                newGrabPoints[0] = gameObjectsMeshCollider;

                gameObjectsGrabbable.Initialize(newGrabPoints);

                gameObjectsGrabbable.enabled = true;
            }


            System.Random random = new System.Random();
            int randomNumber = random.Next(0,2);
            if (randomNumber == 0){
                gameObjectsRigidBody.AddForce(transform.right * 50);
            }
            else if (randomNumber == 1){
                gameObjectsRigidBody.AddForce(-transform.right * 50);
            }
            

            int randomChanceToDestroy = random.Next(0,10);
            if (randomChanceToDestroy < 3 && isTreeDown == false){
                isTreeDown = true;
                Rigidbody treeRigidBody = gameObject.AddComponent<Rigidbody>();
                treeRigidBody.mass = 10000;
                treeRigidBody.AddForce(-transform.forward * 1000);
            }
            randomChanceToDestroy = random.Next(0,10);
            if (randomChanceToDestroy < 3 && isTreeDown == true){
                Destroy(gameObject);
            }
            yield return new WaitForSeconds(0.3f);
            canSpawnLog = true;
        }
    }
}
